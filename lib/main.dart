import 'package:flutter/material.dart';
import 'package:khan_shaheem_techtask_201909/repository/recipes_repo.dart';
import 'package:khan_shaheem_techtask_201909/routes/landing_page.dart';

void main() async {
  RecipesRepo recipesRepo = RecipesRepo();
  await recipesRepo.fetchRecipesJsonMap();
  runApp(new MaterialApp(
    title: 'khan_shaheem_techtask_201909',
    home: LandingPage(),
  ));
}
