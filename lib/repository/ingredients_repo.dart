import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:khan_shaheem_techtask_201909/config.dart';
import 'package:khan_shaheem_techtask_201909/models/ingredient.dart';

class IngredientsRepo {
  Map ingredientsMap;
  List<Ingredient> listOfAvailableIngredients = [];
  List<String> listOfUsableIngredientsTitle = [];

  static final IngredientsRepo _ingredientsRepo = IngredientsRepo.internal();

  factory IngredientsRepo() {
    return _ingredientsRepo;
  }

  IngredientsRepo.internal();

  Future<void> fetchIngredientsJsonMap() async {
    String fileString = await rootBundle.loadString(ingredientsJsonLocation);
    ingredientsMap = await jsonDecode(fileString);
    List listOfAvailableIngredientsMap = ingredientsMap['ingredients'];
    for (Map availableIngredientsMap in listOfAvailableIngredientsMap) {
      Ingredient ingredient = Ingredient.fromJson(availableIngredientsMap);
      //Usable Ingredients
      if (ingredient.useBy.isAfter(DateTime.now())) {
        //Putting ingredients past their best-before date, but within its use-by date
        //to the bottom of the response object its not sorted by date but at the bottom
        if (ingredient.bestBefore.isAfter(DateTime.now())) {
          listOfUsableIngredientsTitle.insert(
              listOfUsableIngredientsTitle.length, ingredient.title);
        } else {
          listOfUsableIngredientsTitle.insert(0, ingredient.title);
        }
      }
      listOfAvailableIngredients.add(ingredient);
    }
  }
}
