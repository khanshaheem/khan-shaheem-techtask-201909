import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:khan_shaheem_techtask_201909/config.dart';
import 'package:khan_shaheem_techtask_201909/models/ingredient.dart';
import 'package:khan_shaheem_techtask_201909/models/recipe.dart';
import 'package:khan_shaheem_techtask_201909/repository/ingredients_repo.dart';

class RecipesRepo {
  Map recipeMap;
  List<Recipe> listOfRecipes = [];
  List<Recipe> listOfValidRecipes = [];

  static final RecipesRepo _recipesRepo = RecipesRepo.internal();

  factory RecipesRepo() {
    return _recipesRepo;
  }

  RecipesRepo.internal();

  Future<void> fetchRecipesJsonMap() async {
    IngredientsRepo ingredientsRepo = IngredientsRepo();
    await ingredientsRepo.fetchIngredientsJsonMap();
    String fileString = await rootBundle.loadString(recipesJsonLocation);
    recipeMap = await jsonDecode(fileString);
    List listOfAvailableRecipesMap = recipeMap['recipes'];
    for (Map availableRecipesMap in listOfAvailableRecipesMap) {
      Recipe recipe = Recipe.fromJson(availableRecipesMap);
      List<Ingredient> listOfRecipeIngredients = recipe.ingredients;
      bool validRecipe = true;
      for (Ingredient recipeIngredient in listOfRecipeIngredients) {
        if (!ingredientsRepo.listOfUsableIngredientsTitle
            .contains(recipeIngredient.title)) {
          validRecipe = false;
        }
      }
      if (validRecipe) {
        listOfValidRecipes.add(recipe);
      }
      listOfRecipes.add(recipe);
    }
  }
}
