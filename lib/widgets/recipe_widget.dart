import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:khan_shaheem_techtask_201909/models/recipe.dart';

class RecipeWidget extends StatefulWidget {
  final Recipe recipe;

  RecipeWidget(this.recipe);

  @override
  _RecipeWidgetState createState() => _RecipeWidgetState(this.recipe);
}

class _RecipeWidgetState extends State<RecipeWidget> {
  Recipe recipe;
  bool notExpanded = true;

  _RecipeWidgetState(this.recipe);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            left: 16,
            right: 12,
          ),
          child: Column(
            children: <Widget>[
              FlatButton(
                child: Container(
                  height: 40,
                  child: new Row(
                    children: <Widget>[
                      Expanded(
                        flex: 10,
                        child: Text(recipe.title,
                            style: const TextStyle(
                                color: const Color(0xff000000),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Roboto",
                                fontStyle: FontStyle.normal,
                                fontSize: 16.0)),
                      ),
                      Expanded(
                          child: notExpanded
                              ? Icon(Icons.keyboard_arrow_down)
                              : Icon(Icons.keyboard_arrow_up)),
                    ],
                  ),
                ),
                onPressed: () {
                  setState(() {
                    notExpanded = !notExpanded;
                  });
                },
              ),
              Offstage(
                offstage: notExpanded,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 16,
                  ),
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: recipe.ingredients == null
                          ? 0
                          : recipe.ingredients.length,
                      itemBuilder: (context, index) {
                        return Text(recipe.ingredients[index].title);
                      }),
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: Colors.grey,
        ),
      ],
    );
  }
}
