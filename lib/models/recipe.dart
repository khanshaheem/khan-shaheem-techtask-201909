import 'ingredient.dart';

class Recipe {
  String title;
  List<Ingredient> ingredients;

  Recipe(this.title, this.ingredients);

  Recipe.fromJson(Map json) {
    this.title = json['title'];
    this.ingredients = [];
    List ingredientsMap = json['ingredients'];
    for (String ingredientMap in ingredientsMap) {
      this.ingredients.add(Ingredient(ingredientMap));
    }
  }
}
