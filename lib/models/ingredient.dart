class Ingredient {
  String title;
  DateTime bestBefore;
  DateTime useBy;

  Ingredient(this.title, {this.bestBefore, this.useBy});

  Ingredient.fromJson(Map json) {
    this.title = json['title'];
    this.bestBefore = DateTime.parse(json['best-before']);
    this.useBy = DateTime.parse(json['use-by']);
  }
}
