import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:khan_shaheem_techtask_201909/models/recipe.dart';
import 'package:khan_shaheem_techtask_201909/repository/recipes_repo.dart';
import 'package:khan_shaheem_techtask_201909/widgets/recipe_widget.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  RecipesRepo recipesRepo = RecipesRepo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text.rich(TextSpan(
                text: 'What can you cook?',
                style: const TextStyle(
                    color: const Color(0xff333333),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Roboto",
                    fontStyle: FontStyle.normal,
                    fontSize: 24.0))),
          ),
          Divider(
            color: Colors.grey,
          ),
          Expanded(
            child: ListView.builder(
                itemCount: recipesRepo.listOfValidRecipes == null
                    ? 0
                    : recipesRepo.listOfValidRecipes.length,
                itemBuilder: (context, index) {
                  Recipe recipe = recipesRepo.listOfValidRecipes[index];
                  return RecipeWidget(recipe);
                }),
          ),
        ],
      ),
    ));
  }
}
