import 'ingredients_repo_test.dart' as ingredientsTests;
import 'recipes_repo_test.dart' as recipesTest;

void main() async {
  await ingredientsTests.main();
  await recipesTest.main();
}