import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:khan_shaheem_techtask_201909/models/ingredient.dart';
import 'package:khan_shaheem_techtask_201909/repository/ingredients_repo.dart';

Future<void> main() async {
  group("ingredients repo test group", () {
    setUp(() async {
      //override rootBundle functionality for reading json
      defaultBinaryMessenger.setMockMessageHandler('flutter/assets', (message) {
        String key = utf8.decode(message.buffer.asUint8List());
        var file = new File('$key');
        final Uint8List encoded = utf8.encoder.convert(file.readAsStringSync());
        return Future.value(encoded.buffer.asByteData());
      });
    });

    test('test json fetch', () async {
      IngredientsRepo ingredientsRepo = IngredientsRepo();
      await ingredientsRepo.fetchIngredientsJsonMap();
      expect(
          jsonEncode(ingredientsRepo.ingredientsMap),
          jsonEncode({
            "ingredients": [
              {
                "title": "Ham",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Cheese",
                "best-before": "2019-09-08",
                "use-by": "2019-09-13"
              },
              {
                "title": "Bread",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Butter",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Bacon",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Eggs",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Mushrooms",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Sausage",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Hotdog Bun",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Ketchup",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Mustard",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Lettuce",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Tomato",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Cucumber",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Beetroot",
                "best-before": "2019-09-25",
                "use-by": "2019-09-27"
              },
              {
                "title": "Salad Dressing",
                "best-before": "2019-09-06",
                "use-by": "2019-09-07"
              }
            ]
          }));
    });

    test('test fromJson Ingredient constructor', () async {
      Map ingredientMap = {
        "title": "Salad Dressing",
        "best-before": "2019-09-06",
        "use-by": "2019-09-07"
      };
      Ingredient ingredient = Ingredient.fromJson(ingredientMap);
      expect(ingredient.title, ingredientMap['title']);
      DateTime bestBefore = ingredient.bestBefore;
      expect("${bestBefore.year}-0${bestBefore.month}-0${bestBefore.day}",
          ingredientMap['best-before']);
      DateTime useBy = ingredient.useBy;
      expect("${useBy.year}-0${useBy.month}-0${useBy.day}",
          ingredientMap['use-by']);
    });

    test('test listOfAvailableIngredients is not empty', () async {
      IngredientsRepo ingredientsRepo = IngredientsRepo();
      expect(ingredientsRepo.listOfAvailableIngredients.length != 0, true);
    });

    test(
        'test listOfUsableIngredients is less than or equal listOfAvailableIngredients',
        () async {
      IngredientsRepo ingredientsRepo = IngredientsRepo();
      expect(
          ingredientsRepo.listOfUsableIngredientsTitle.length <=
              ingredientsRepo.listOfAvailableIngredients.length,
          true);
    });
  });
}
