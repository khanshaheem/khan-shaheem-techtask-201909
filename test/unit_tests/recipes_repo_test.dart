import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:khan_shaheem_techtask_201909/models/recipe.dart';
import 'package:khan_shaheem_techtask_201909/repository/recipes_repo.dart';

Future<void> main() async {
  group("recipes repo test group", () {
    setUp(() async {
      //override rootBundle functionality for reading json
      defaultBinaryMessenger.setMockMessageHandler('flutter/assets', (message) {
        String key = utf8.decode(message.buffer.asUint8List());
        var file = new File('$key');
        final Uint8List encoded = utf8.encoder.convert(file.readAsStringSync());
        return Future.value(encoded.buffer.asByteData());
      });
    });

    test('test json fetch', () async {
      RecipesRepo recipesRepo = RecipesRepo();
      await recipesRepo.fetchRecipesJsonMap();
      expect(
          jsonEncode(recipesRepo.recipeMap),
          jsonEncode({
            "recipes": [
              {
                "title": "Ham and Cheese Toastie",
                "ingredients": ["Ham", "Cheese", "Bread", "Butter"]
              },
              {
                "title": "Fry-up",
                "ingredients": [
                  "Bacon",
                  "Eggs",
                  "Baked Beans",
                  "Mushrooms",
                  "Sausage",
                  "Bread"
                ]
              },
              {
                "title": "Salad",
                "ingredients": [
                  "Lettuce",
                  "Tomato",
                  "Cucumber",
                  "Beetroot",
                  "Salad Dressing"
                ]
              },
              {
                "title": "Hotdog",
                "ingredients": ["Hotdog Bun", "Sausage", "Ketchup", "Mustard"]
              }
            ]
          }));
    });

    test('test fromJson Recipe constructor', () async {
      Map recipeMap = {
        "title": "Hotdog",
        "ingredients": ["Hotdog Bun", "Sausage", "Ketchup", "Mustard"]
      };
      Recipe recipe = Recipe.fromJson(recipeMap);
      expect(recipe.title, recipeMap['title']);
      expect(recipe.ingredients.length, 4);
    });

    test('test recipes is not empty', () async {
      RecipesRepo recipesRepo = RecipesRepo();
      expect(recipesRepo.listOfRecipes.length != 0, true);
    });

    test('test validRecipes is less than or equal to recipes', () async {
      RecipesRepo recipesRepo = RecipesRepo();
      expect(
          recipesRepo.listOfValidRecipes.length <=
              recipesRepo.listOfRecipes.length,
          true);
    });
  });
}
