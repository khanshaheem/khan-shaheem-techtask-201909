# khan_shaheem_techtask_201909

flutter test for ukufu see Engineering_Tech_Task.pdf file for instructions

# Build Commands

Android

    flutter build apk -t lib/main.dart --release 
    
iOS
	
    flutter build ios -t lib/main.dart --release 
    cd ios/ 
    open Runner.xcworkspace
    **archive build**

# Run Commands

    flutter run -t lib/main.dart
    

# Unit Test Command to run all unit tests

    flutter test --coverage test/unit_tests/index.dart